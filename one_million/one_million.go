package main

import "fmt"

func main() {
	for i := 0; i < 1000000; i++ {
		fmt.Println("Hello, this is iteration number: ", i, "\n")
	}
}
